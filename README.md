# Matrix Hacks

Contribution and hacks to matrix related projects. Matrix can be found at http://www.matrix.org and codebase at  https://github.com/matrix-org

#Create a user in homeserver

> docker exec -it synapse sh
> register_new_matrix_user -c /compiled/homeserver.yaml
